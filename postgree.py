import config
import psycopg2
conn_string = "dbname='" + config.PostGreeWappa['database'] + "' port='" + config.PostGreeWappa['port'] + "' user='" + \
              config.PostGreeWappa['username'] + "' password='" + config.PostGreeWappa['password'] + "' host='" + \
              config.PostGreeWappa['host'] + "' connect_timeout=3000 "


def atualizarStatusCorrida(rideid, status) :
    list = []
    con = psycopg2.connect(conn_string)
    cur = con.cursor()
    sql = "update rideb2c.ride rd set status='{0}' where rideid={1} ".format(status, rideid)
    cur.execute(sql)
    con.commit()
    con.close()

def retornarListaCorridaDerrubarAwaitingPickup() :
    list = []
    con = psycopg2.connect(conn_string)
    cur = con.cursor()
    sql = " select  rd.rideid, rd.userid , ac.email from rideb2c.ride rd inner join personal.account ac on rd.userid = ac.userid where rd.status='AwaitingPickup' and updatedat < ( current_timestamp - interval '120 min' )   order by rideid asc  "
    cur.execute(sql)

    for result in cur:
        item = ride(str(result[0]), str(result[1]), str(result[2]))
        list.append (item)

    return  list
def retornarListaCorridaDerrubarOnAction() :

    list = []
    con = psycopg2.connect(conn_string)
    cur = con.cursor()
    sql = " select    rd.rideid, rd.userid ,  ac.email from rideb2c.ride rd inner join personal.account ac on rd.userid = ac.userid  where rd.status='OnAuction' and updatedat < ( current_timestamp - interval '10 min' )  order by rideid asc "

    cur.execute(sql)

    for result in cur:
        item = ride(str(result[0]), str(result[1]), str(result[2]))
        list.append (item)

    return  list

def retornarListaCorridaDerrubarAwaitingPickupDia() :
    list = []
    con = psycopg2.connect(conn_string)
    cur = con.cursor()
    sql = " select  rd.rideid, rd.userid , ac.email from rideb2c.ride rd inner join personal.account ac on rd.userid = ac.userid where rd.status='AwaitingPickup' and updatedat < ( current_timestamp - interval '1 day' )   order by rideid asc  "
    cur.execute(sql)

    for result in cur:
        item = ride(str(result[0]), str(result[1]), str(result[2]))
        list.append (item)

    return  list


def retornarListaCorridaDerrubarOnActionDia() :

    list = []
    con = psycopg2.connect(conn_string)
    cur = con.cursor()
    sql = " select    rd.rideid, rd.userid ,  ac.email from rideb2c.ride rd inner join personal.account ac on rd.userid = ac.userid  where rd.status='OnAuction' and updatedat < ( current_timestamp - interval '1 day' )  order by rideid asc "

    cur.execute(sql)

    for result in cur:
        item = ride(str(result[0]), str(result[1]), str(result[2]))
        list.append (item)

    return  list

def retornarListaCorridaDerrubarByRideid(rideid):
    list = []
    con = psycopg2.connect(conn_string)
    cur = con.cursor()
    sql = " select    rd.rideid, rd.userid ,  ac.email from rideb2c.ride rd inner join personal.account ac on rd.userid = ac.userid where rd.rideid={0} order by rideid asc ".format(rideid)

    cur.execute(sql)

    for result in cur:
        item = ride(str(result[0]), str(result[1]), str(result[2]))
        list.append(item)

    return list
class ride(object):
    def __init__(self, rideid, userid,email):
        self.rideid = rideid
        self.userid = userid
        self.email = email
