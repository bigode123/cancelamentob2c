import config
import postgree
import oracle
import requestapi
import io
import datetime

# derrubar corridas em AwaitingPickup
lista = postgree.retornarListaCorridaDerrubarAwaitingPickup()

data =  datetime.datetime.now()

arquivo  = 'c://temp//corridaderrubada_{0}.txt'.format(data.strftime('%d%m%Y') )


def gerarArquivo(filename, linha):
    file = io.open(filename, encoding="ascii", mode="a")
    file.writelines(linha + "\n")
    file.close()


if (lista.__len__() > 0 ) :
    print (' DerrubarAwaitingPickup  => ')
    retornoora = oracle.retornarStatusRide(lista)
    for elemento in retornoora :
        if ( elemento[2]==0):
            print('rideId =>' +  str(elemento[0]))
            retorno = requestapi.derrubarcorrida(elemento[0], elemento[1])
            if (retorno < 300) :
                gerarArquivo(arquivo, elemento[3])
        else:
            print (' Existe corrida no oracle =>  {0} '.format(elemento[0]) )



# derrubar corridas em OnAction
lista = postgree.retornarListaCorridaDerrubarOnAction()

if (lista.__len__() > 0 ) :
    print (' DerrubarOnAction   => ')
    retornoora = oracle.retornarStatusRide(lista)
    for elemento in retornoora :
        if ( elemento[2]==0):
            print('rideId =>' +  str(elemento[0]))
            retorno=requestapi.derrubarcorrida(elemento[0], elemento[1])

            if (retorno < 300) :
                gerarArquivo(arquivo, elemento[3])
        else:
            print (' Existe corrida no oracle =>  {0}'.format(elemento[0]) )

# derrubar corridas em Rideid
rideid=0
if (rideid>0) :
    lista = postgree.retornarListaCorridaDerrubarByRideid(rideid)

    if (lista.__len__() > 0):
        print(' Derrubar Por RideId   => ')
        retornoora = oracle.retornarStatusRideById(lista)
        for elemento in retornoora:
            if (elemento[2] == 0):
                print('rideId =>' + str(elemento[0]))
                retorno = requestapi.derrubarcorrida(elemento[0], elemento[1])

                if (retorno < 300):
                    gerarArquivo(arquivo, elemento[3])
            else:
                print(' Existe corrida no oracle =>  {0}'.format(elemento[0]))

##################################################################################################
### Derrubar corridas com status em aberto no oracle

lista = postgree.retornarListaCorridaDerrubarAwaitingPickupDia()

arquivo  = 'c://temp//corridaderrubada_{0}.txt'.format(data.strftime('%d%m%Y') )

if (lista.__len__() > 0 ) :
    print (' Derrubar Awainting Pickup no oracle    => ')
    retornoora = oracle.retornarStatusRideAberto(lista)
    for elemento in retornoora :
        if ( elemento[2]>0):
            print('rideId =>' +  str(elemento[0]))
            retorno = requestapi.derrubarcorrida(elemento[0], elemento[1])
            oracle.cancelarcorridaoracle (elemento[0])
            if (retorno < 300) :
                gerarArquivo(arquivo, elemento[3])


lista = postgree.retornarListaCorridaDerrubarOnActionDia()



arquivo  = 'c://temp//corridaderrubada_{0}.txt'.format(data.strftime('%d%m%Y') )

if (lista.__len__() > 0 ) :
    print (' Derrubar On Action no oracle    => ')

    retornoora = oracle.retornarStatusRideAberto(lista)
    for elemento in retornoora :
        if ( elemento[2]>0):
            print('rideId =>' +  str(elemento[0]))
            retorno = requestapi.derrubarcorrida(elemento[0], elemento[1])
            oracle.cancelarcorridaoracle (elemento[0])
            if (retorno < 300) :
                gerarArquivo(arquivo, elemento[3])


#####################################################################################################
### Derrubar corridas com status de fechadao

lista = postgree.retornarListaCorridaDerrubarAwaitingPickupDia()

arquivo  = 'c://temp//corridaderrubada_{0}.txt'.format(data.strftime('%d%m%Y') )

if (lista.__len__() > 0 ) :

    print (' Derrubar Fechada Awainting Pickup no oracle    => ')

    retornoora = oracle.retornarStatusRideFechado(lista)
    for elemento in retornoora :
        if ( elemento[2]>0):
            print('rideId =>' +  str(elemento[0]))
            postgree.atualizarStatusCorrida(elemento[0], 'Started')
            retorno = requestapi.finishride(elemento[0], elemento[1], elemento[4], elemento[5])

            if (retorno < 300) :
                gerarArquivo(arquivo, elemento[3])


lista = postgree.retornarListaCorridaDerrubarOnActionDia()



arquivo  = 'c://temp//corridaderrubada_{0}.txt'.format(data.strftime('%d%m%Y') )

if (lista.__len__() > 0 ) :
    print (' Derrubar Fechada Action Pickup no oracle    => ')
    retornoora = oracle.retornarStatusRideFechado(lista)
    for elemento in retornoora :
        if ( elemento[2]>0):
            print('rideId =>' +  str(elemento[0]))
            postgree.atualizarStatusCorrida(elemento[0], 'Started')
            retorno = requestapi.finishride(elemento[0], elemento[1], elemento[4], elemento[5])

            if (retorno < 300) :
                gerarArquivo(arquivo, elemento[3])



def retornarEspaco(campo, quantidade):
    retorno =  (' ' * quantidade) + campo
    return retorno

def tratarCampoNumerico (campo) :
    retorno =str(campo).replace('#', '')
    if campo is  None  or campo =='None':
        retorno ='0'
    return retorno
def tratarCampo (campo) :
    retorno =str(campo).replace('#', '')
    if campo is  None :
        retorno =''

    return retorno