import requests
import json
def derrubarcorrida(rideid, userid):
    urlapi = "http://ride-b2c-api.wappa.com.br/api/ride/cancel/{0}/{1}".format( userid,rideid)
    data = {"type" : "system"}
    data_json = json.dumps(data)
    headersjson = {'Content-type': 'application/json'}
    resp = requests.post(url=urlapi, data=data_json, headers=headersjson)
    return resp.status_code

def finishride (rideid, userid, latitude, longitude) :

    urlapi = "http://ride-b2c-api.wappa.com.br/api/ride/finish/{0}/{1}".format( userid,rideid)
    data = {
    "dropoff":{
        "latitude": str(latitude) ,
        "longitude": str(longitude)
    }
    }
    data_json = json.dumps(data)
    headersjson = {'Content-type': 'application/json'}
    resp = requests.post(url=urlapi, data=data_json, headers=headersjson)
    return resp.status_code